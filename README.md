# TrabajoMaestria
Descripción
Este sistema de presupuestos mensual de gastos e ingresos con PHP y MySQL está diseñado para todas aquellas personas o pequeñas empresas que desean llevar un registro detallado de su presupuesto mensual de gastos e ingresos.

Hacer un presupuesto es el paso más importante que puedes tomar para salir de la deuda con tu tarjeta de crédito más rápido, sin embargo, el alto porcentaje de personas con alta deuda muestra cuán pocos parecen darse cuenta de eso.

Qué es exactamente el presupuesto? En su forma más simple, es un libro de contabilidad que detalla las decisiones de gasto que piensas tomar. Calcula cuánto dinero recibirá en los próximos meses y asigna suficiente dinero para cubrir gastos como alimentos, vivienda, transporte y seguros. Un buen presupuesto también incluye asignaciones para ahorros regulares.

Características
Creación de presupuestos mediante interfaz web.
Los presupuestos pueden englobar múltiples conceptos de gastos e ingresos.
Cálculo automático del monto total del presupuesto.
Diseño responsive, se adapta a cualquier tamaño de dispositivo.
Filtrar presupuesto de meses anteriores
Almacenamiento de los presupuestos en una base de datos MySQL.


Descarga y uso:
Instalar Docker
Descargar los archivos fuentes del sistema.
Iniciar Docker-compose
Crear una base de datos usando PHPMyAdmin accediendo a la url siguiente: http://localhost/phpmyadmin/
la importación de los datos desde PHPMyAdmin
Configurar los datos de conexión a la base de datos editando el archivo de configuración que se encuentra en la siguiente ruta: src/config/db.php
Vista web: http://ip:puerto con el cual se expuso/presupuesto_mensual/

Dos versiones:
VersionPHPcincoseis
VersionPHPsiete